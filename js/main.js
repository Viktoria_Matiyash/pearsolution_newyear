function startAnimation() {
    $(".frame").addClass("animate-pm"), $(".ready__content").addClass("animate-form"), $(".ready__form").hide(), $(".ready__caption").css("display", "none"), $("#animation").css("background", "transparent"), setTimeout(function () {
        $(".ready__icon").addClass("animate-plane"), $("#animation").css("display", "none")
    }, 1700), setTimeout(function () {
        $(".js-successfully").fadeIn("slow")
    }, 2800)
}

function contactsAnimation() {
    $(".contacts__request").addClass("animate-cont"), setTimeout(function () {
        $(".contacts__flap").removeClass("js-letter"), $(".contacts__flap").addClass("letter-my__flap--color"), $(".contacts__request").css("display", "none"), $(".contacts__icon--open").css("display", "none"), $(".contacts__top").css("display", "none"), $(".contacts__icon--close").css("display", "block")
    }, 1400), setTimeout(function () {
        $(".contacts__flap").removeClass("js-letter"), $(".contacts__flap").addClass("letter-my__flap--color")
    }, 1100), setTimeout(function () {
        $(".envelope__answer").fadeToggle(800)
    }, 1800)
}

function sendComment() {
    $(".blog-page__form").hide(), $(".blog-page__good-send").fadeOut("slow"), $(".blog-page__good-send").css("display", "flex")
}

$.ajaxSetup({headers: {"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")}}), $(".adaptive-menu__open-menu").click(function () {
    $(".adaptive-menu__full-menu").slideDown()
}), $(".adaptive-menu__close-menu").click(() => {
    $(".adaptive-menu__full-menu").slideUp()
}), $(".carousel-indicators  li").on("mouseover", function () {
    $(this).trigger("click")
}), $("#secondSlider").carousel({avtoHeight: !1}), $("#mainSlider").carousel({avtoHeight: !1}), $(".js-hover").hover(function () {
    $(this).addClass("slider__active")
}, function () {
    $(this).removeClass("slider__active")
}), $(function () {
    $(".js-mask").mask("+38(000)000-00-00")
}), $(".contacts__send").click(function () {
    $(".envelope__flap").removeClass("envelope-js"), setTimeout(function () {
        $(".envelope__flap").addClass("envelope__flap--color")
    }, 300)
}), $(function () {
    $(".blog__selection .js-sel").click(function (e) {
        e.preventDefault(), $(".blog__selection .js-sel").removeClass("active"), $(this).addClass("active")
    })
}), $(function () {
    $(".js-filters-active").click(function (e) {
        e.preventDefault(), $(".js-filters-active").removeClass("active"), $(this).addClass("active")
    })
}), $(".ready__form").on("submit", function (e) {
    e.preventDefault(), $.ajax({
        type: "POST", url: "/", data: $(this).serialize(), success: function (e) {
            startAnimation()
        }, error: function (e) {
            alert("Введите данные правильно")
        }
    })
}), $(".contacts__form").on("submit", function (e) {
    e.preventDefault(), $.ajax({
        type: "POST", url: "/", data: $(this).serialize(), success: function (e) {
            contactsAnimation()
        }, error: function (e) {
            alert("Введите данные правильно")
        }
    })
}), $(".blog-page__form").on("submit", function (e) {
    e.preventDefault(), $.ajax({
        type: "POST", url: "/", data: $(this).serialize(), success: function (e) {
            sendComment()
        }, error: function (e) {
            alert("Введите данные правильно")
        }
    })
});